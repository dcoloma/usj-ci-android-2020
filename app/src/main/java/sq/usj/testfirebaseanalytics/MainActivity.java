package sq.usj.testfirebaseanalytics;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bLogin = findViewById(R.id.buttonLogin);
        bLogin.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "click on login";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });

        Button bSubscribe = findViewById(R.id.buttonSubscribe);
        bSubscribe.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "click on subscribe";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });

        Button bTrial = findViewById(R.id.buttonTrial);
        bTrial.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "click on trial";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });

        Button bShare = findViewById(R.id.buttonShare);
        bShare.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "click on share";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });

    }
}
